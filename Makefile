all: build/iperf3-client build/iperf3-server

.PHONY: clean
clean:
	rm -rf build

build/iperf3-client: cmd/client/*.go pkg/config/*.go
	CGO_ENABLED=0 go build -o $@ cmd/client/*.go

build/iperf3-server: cmd/server/*.go pkg/config/*.go
	CGO_ENABLED=0 go build -o $@ cmd/server/*.go
