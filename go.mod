module gitlab.com/mergetb/exptools/forensics/iperf

go 1.18

require (
	gitlab.com/mergetb/exptools/forensics/measurement v0.0.0-20221116175853-0841cf94bf9c
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/BGrewell/go-conversions v0.0.0-20201203155646-5e189e4ca087 // indirect
	github.com/BGrewell/go-iperf v0.0.0-20220419142523-6b8246947c28 // indirect
	github.com/BGrewell/tail v1.0.0 // indirect
	github.com/deepmap/oapi-codegen v1.8.2 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/uuid v1.1.2 // indirect
	github.com/hpcloud/tail v1.0.0 // indirect
	github.com/influxdata/influxdb-client-go/v2 v2.12.0 // indirect
	github.com/influxdata/line-protocol v0.0.0-20200327222509-2487e7298839 // indirect
	github.com/mergetb/go-iperf v0.0.0-20221116034024-42a03507bbf3 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/net v0.0.0-20210119194325-5f4716e94777 // indirect
	golang.org/x/sys v0.0.0-20210124154548-22da62e12c0c // indirect
	golang.org/x/text v0.3.5 // indirect
	google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013 // indirect
	google.golang.org/grpc v1.36.1 // indirect
	google.golang.org/protobuf v1.26.0 // indirect
	gopkg.in/fsnotify.v1 v1.4.7 // indirect
	gopkg.in/tomb.v1 v1.0.0-20141024135613-dd632973f1e7 // indirect
)
