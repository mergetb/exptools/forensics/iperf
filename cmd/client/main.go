package main

import (
	"flag"
	"fmt"
	"log"
	"math"
	"net"
	"os"
	"time"

	"github.com/mergetb/go-iperf"

	"gitlab.com/mergetb/exptools/forensics/iperf/internal"
	"gitlab.com/mergetb/exptools/forensics/iperf/pkg/config"
	"gitlab.com/mergetb/exptools/forensics/measurement/pkg/influx"
	"gitlab.com/mergetb/exptools/forensics/measurement/pkg/sampler"
)

const (
	defaultConf string = "/etc/mergetb/iperf.yml"
)

var (
	client string
	server string
	tags   map[string]string
)

func main() {
	var conf string
	var debug bool

	flag.StringVar(&conf, "config", defaultConf, "path to configuration file")
	flag.BoolVar(&debug, "debug", false, "enabling debug messages")
	flag.Parse()

	cfg, err := config.GetConfigFromYaml(conf)
	if err != nil {
		log.Fatal(err)
	}
	cli := &cfg.Client.ClientOptions

	host, err := parseAddr(*cli.Host)
	if err != nil {
		log.Fatal(err)
	}

	client = internal.GetHostname()
	server = *cli.Host
	tags = cfg.Tags

	c := iperf.NewClient(host)
	loadOptions(c, cli)
	c.Debug = debug

	err = c.Start()
	if err != nil {
		log.Fatalf("failed to start client: %v\n", err)
	}

	<-c.Done

	if *c.ExitCode() != 0 {
		data, err := os.ReadFile(c.LogFile())
		if err == nil {
			log.Print(string(data))
		}
		os.Exit(*c.ExitCode())
	}

	// create database
	db, err := influx.NewDatabase(cfg.InfluxServer, cfg.InfluxDatabase)
	if err != nil {
		log.Fatal(err)
	}

	// start channel writer
	go db.WriteChannel()

	// report
	rep := c.Report()

	if c.Proto() == iperf.PROTO_UDP {
		// Grab UDP data from the server, as it gives us per-interval drop and
		// out-of-order metrics unavailable to the client
		srv := rep.ServerOutputJson
		for _, in := range srv.Intervals {
			pt := formatPointUDP(&rep.Start.Timestamp, in.Sum)
			db.Channel <- pt
		}
	} else {
		// Grab TCP from the client
		for _, in := range rep.Intervals {
			pt := formatPointTCP(&rep.Start.Timestamp, in.Sum)
			db.Channel <- pt
		}
	}

	log.Printf("client exited successfully")
}

func formatPointUDP(ts *iperf.TimestampInfo, rep *iperf.StreamIntervalSumReport) *sampler.Point {
	sec, dec := math.Modf(float64(rep.EndInterval))
	sec += float64(ts.TimeSecs)
	t := time.Unix(int64(sec), int64(dec*(1e9)))

	pt := &sampler.Point{
		Name: "iperf3",
		Tags: map[string]string{
			"client": client,
			"server": server,
			"proto":  "udp",
		},
		Fields: map[string]interface{}{
			"start":           rep.StartInterval,
			"end":             rep.EndInterval,
			"seconds":         rep.Seconds,
			"bytes":           rep.Bytes,
			"bits_per_second": rep.BitsPerSecond,
			"omitted":         rep.Omitted,
			"jitter_ms":       rep.JitterMs,
			"lost_packets":    rep.LostPackets,
			"packets":         rep.Packets,
			"sender":          rep.Sender,
		},
		Timestamp: t,
	}

	for k, v := range tags {
		pt.Tags[k] = v
	}

	return pt
}

func formatPointTCP(ts *iperf.TimestampInfo, rep *iperf.StreamIntervalSumReport) *sampler.Point {
	sec, dec := math.Modf(float64(rep.EndInterval))
	sec += float64(ts.TimeSecs)
	t := time.Unix(int64(sec), int64(dec*(1e9)))

	pt := &sampler.Point{
		Name: "iperf3",
		Tags: map[string]string{
			"client": client,
			"server": server,
			"proto":  "tcp",
		},
		Fields: map[string]interface{}{
			"start":           rep.StartInterval,
			"end":             rep.EndInterval,
			"seconds":         rep.Seconds,
			"bytes":           rep.Bytes,
			"bits_per_second": rep.BitsPerSecond,
			"omitted":         rep.Omitted,
			"retransmits":     rep.Retransmissions,
		},
		Timestamp: t,
	}

	for k, v := range tags {
		pt.Tags[k] = v
	}

	return pt

}

func parseAddr(host string) (string, error) {
	ip := net.ParseIP(host)
	if ip != nil {
		return host, nil
	}

	ips, err := net.LookupIP(host)
	if err != nil {
		return "", fmt.Errorf("cannot find ipv4 address for host=%s", host)
	}

	for _, ip := range ips {
		if ip.To4() != nil {
			return ip.String(), nil
		}
	}

	return "", fmt.Errorf("cannot find ipv4 address for host=%s", host)
}

func loadOptions(c *iperf.Client, opts *iperf.ClientOptions) {
	if opts.Port != nil {
		c.SetPort(*opts.Port)
	}

	if opts.Format != nil {
		c.SetFormat(*opts.Format)
	}

	if opts.Interval != nil {
		c.SetInterval(*opts.Interval)
	}

	if opts.Proto != nil {
		c.SetProto(*opts.Proto)
	}

	if opts.Bandwidth != nil {
		c.SetBandwidth(*opts.Bandwidth)
	}

	if opts.TimeSec != nil {
		c.SetTimeSec(*opts.TimeSec)
	}

	if opts.BlockCount != nil {
		c.SetBlockCount(*opts.BlockCount)
	}

	if opts.Length != nil {
		c.SetLength(*opts.Length)
	} else if c.Proto() == iperf.PROTO_UDP {
		c.SetLength("1460")
	} else {
		c.SetLength("128K")
	}

	if opts.Streams != nil {
		c.SetStreams(*opts.Streams)
	}

	if opts.Window != nil {
		c.SetWindow(*opts.Window)
	}

	if opts.MSS != nil {
		c.SetMSS(*opts.MSS)
	}

	if opts.NoDelay != nil {
		c.SetNoDelay(*opts.NoDelay)
	}

	if opts.TOS != nil {
		c.SetTOS(*opts.TOS)
	}

	if opts.ZeroCopy != nil {
		c.SetZeroCopy(*opts.ZeroCopy)
	}

	if opts.OmitSec != nil {
		c.SetOmitSec(*opts.OmitSec)
	}

	if opts.Prefix != nil {
		c.SetPrefix(*opts.Prefix)
	}

	// required
	c.SetVersion4(true)
	c.SetJSON(true)
	c.SetIncludeServer(true)
}
