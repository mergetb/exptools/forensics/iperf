package main

import (
	"flag"
	"log"
	"os"
	"time"

	"github.com/mergetb/go-iperf"

	"gitlab.com/mergetb/exptools/forensics/iperf/pkg/config"
)

const (
	defaultConf string = "/etc/mergetb/iperf.yml"
)

func main() {
	var conf string
	var debug bool

	flag.StringVar(&conf, "config", defaultConf, "path to configuration file")
	flag.BoolVar(&debug, "debug", false, "enabling debug messages")
	flag.Parse()

	cfg, err := config.GetConfigFromYaml(conf)
	if err != nil {
		log.Fatal(err)
	}
	srv := &cfg.Server.ServerOptions

	s := iperf.NewServer()
	loadOptions(s, srv)
	s.Debug = debug

	err = s.Start()
	if err != nil {
		log.Fatalf("failed to start server: %v\n", err)
	}

	for s.Running {
		time.Sleep(time.Second * 1)
	}

	if *s.ExitCode != 0 {
		log.Printf("server exited with code %d", *s.ExitCode)
		os.Exit(*s.ExitCode)
	}

	log.Printf("server exited successfully")
}

func loadOptions(s *iperf.Server, opts *iperf.ServerOptions) {
	if opts.OneOff != nil {
		s.SetOneOff(*opts.OneOff)
	}

	if opts.Port != nil {
		s.SetPort(*opts.Port)
	}
}
