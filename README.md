# Iperf

A simple program that runs iperf3, collects its output, and writes it to an influxdb database.

## Prerequisites

### Compile Prerequisites
- Golang
  - Only tested with version 1.18.7

### Running Prerequisites
You need to have an [InfluxDB](https://www.influxdata.com/) instance running before executing this application. See the
[MergeTB exptools/forensics/deployment](https://gitlab.com/mergetb/exptools/forensics/deployment) for an example of how to set one up.

## HowTo

### Compile
```shell
make
```

### Run
Server:
```shell
./build/iperf3-server -config <config file>
```

Client:
```shell
./build/iperf3-client -config <config file>
```

Example, using [the configuration in this repo](conf/config.yml)

```shell
[briankoco@strahl iperf]$ ./build/iperf3-server -config conf/config.yml &
[1] 483735
[briankoco@strahl iperf]$ ./build/iperf3-client -config conf/config.yml 
2022/11/16 12:00:32 client exited successfully
2022/11/16 12:00:32 server exited successfully
[1]+  Done                    ./build/iperf3-server -config conf/config.yml
```

## Configuration

Both the client and server require a configuration file in YAML format. [conf/config.yml](conf/config.yml):
```yml
---
client:
  port: 5201
  host: localhost
  streams: 1
  interval: 1
  time_sec: 5
  proto: tcp 
server:
  port: 5201
  one_off: true
tags: 
    id: test
influx_server: http://localhost:8086
influx_database: forensics
```

The client connects to the server at `localhost:5201`, creates one stream, measures performance every 1 second, runs for a total of 5 seconds, and uses the TCP protocol. The server listens at port `5201` and exits after one client run. The client writes data to an InfluxDB instance at http://localhost:8086, writing its data to a database called `forensics`. Each data point it writes is tagged with the key/value "id": "test".

Generally speaking, the client and server accept the same options as the `iperf3` program. See the full set of options:
- Client options: https://github.com/mergetb/go-iperf/blob/main/client.go#L37-L62
- Server options: https://github.com/mergetb/go-iperf/blob/main/server.go#L31-L38
