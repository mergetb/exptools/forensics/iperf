package config

import (
	"fmt"
	"io/ioutil"
	"log"

	"github.com/mergetb/go-iperf"

	"gopkg.in/yaml.v2"
)

const (
	dflt_influx_server   string = "http://localhost:8086"
	dflt_influx_database string = "forensics"
)

type ServerConfig struct {
	iperf.ServerOptions `yaml:",inline"`
}

type ClientConfig struct {
	iperf.ClientOptions `yaml:",inline"`
}

type Config struct {
	Server         ServerConfig      `yaml:"server"`
	Client         ClientConfig      `yaml:"client"`
	Tags           map[string]string `yaml:"tags"`
	InfluxServer   string            `yaml:"influx_server"`
	InfluxDatabase string            `yaml:"influx_database"`
}

func GetConfigFromYaml(filename string) (*Config, error) {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, fmt.Errorf("read %s: %v", filename, err)
	}

	cfg := new(Config)
	err = yaml.Unmarshal(data, cfg)
	if err != nil {
		return nil, fmt.Errorf("unmarshal %s: %v", filename, err)
	}

	if cfg.InfluxServer == "" {
		log.Printf("config does not define influx_server; defaulting to %s", dflt_influx_server)
		cfg.InfluxServer = dflt_influx_server
	}

	if cfg.InfluxDatabase == "" {
		log.Printf("config does not define influx_database; defaulting to %s", dflt_influx_database)
		cfg.InfluxDatabase = dflt_influx_database
	}

	if cfg.Client.Host == nil {
		return nil, fmt.Errorf("config must define client.Host")
	}

	return cfg, nil
}
